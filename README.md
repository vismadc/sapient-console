# visma/sapient-console

A console application for generating keys with [paragonie/sapient], an amazing library for securing and signing requests.

## Installation

Install using [composer](https://getcomposer.org/) to install the application.

```bash
composer global require visma/sapient-console
```

You should add your composer bin directory to path.

## Usage

```
sapient help
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

[paragonie/sapient]: https://github.com/paragonie/sapient
