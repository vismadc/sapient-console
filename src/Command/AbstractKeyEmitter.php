<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license Proprietary
 * @author Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\SapientConsole\Command;

use ParagonIE\Sapient\CryptographyKey;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractKeyEmitter extends Command
{
    protected function configure()
    {
        $this->addOption("binary", "b", InputOption::VALUE_NONE, "Output the raw binary key");
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @param \ParagonIE\Sapient\CryptographyKey $cryptographyKey
     * @return int
     */
    public function writeKey(InputInterface $input, OutputInterface $output, CryptographyKey $cryptographyKey): int
    {
        if ($input->getOption("binary")) {
            $output->write($cryptographyKey->getString(true), false, OutputInterface::VERBOSITY_QUIET);
            return 0;
        }

        if ($input->getOption("quiet")) {
            $output->write($cryptographyKey->getString(false), false, OutputInterface::VERBOSITY_QUIET);
            return 0;
        }

        $io = new SymfonyStyle($input, $output);
        $io->success(sprintf("Key: %s", $cryptographyKey->getString(false)));
        return 0;
    }
}
