<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license Proprietary
 * @author Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\SapientConsole\Command;

use ParagonIE\Sapient\CryptographyKeys\SealingSecretKey;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateSealingSecretKey extends AbstractKeyEmitter
{
    protected function configure()
    {
        $this->setName("sealing:generate-secret");
        $this->setDescription("Generate a sealing secret key");
        parent::configure();
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $key = SealingSecretKey::generate();

        return $this->writeKey($input, $output, $key);
    }
}
