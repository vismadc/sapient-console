<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license Proprietary
 * @author Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\SapientConsole\Command;

use ParagonIE\Sapient\CryptographyKeys\SharedEncryptionKey;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateSharedEncryptionKey
 */
class GenerateSharedEncryptionKey extends AbstractKeyEmitter
{
    protected function configure()
    {
        $this->setName("shared:generate-encryption");
        $this->setDescription("Generate a shared encryption key");
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $key = SharedEncryptionKey::generate();

        $this->writeKey($input, $output, $key);
    }
}
