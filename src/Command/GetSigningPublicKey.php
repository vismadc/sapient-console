<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license Proprietary
 * @author Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\SapientConsole\Command;

use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Sapient\CryptographyKeys\SigningSecretKey;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetSigningPublicKey extends AbstractKeyConverter
{
    protected function configure()
    {
        $this->setName("signing:get-public");
        $this->setDescription("Get a the public key for a private one");
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $key = $this->getKey($input);

        if ($input->getOption("raw")) {
            $signingSecretKey = new SigningSecretKey($key);
        } else {
            $key = Base64UrlSafe::decode($key);
            echo strlen($key);
            $signingSecretKey = new SigningSecretKey($key);
        }

        $signingPublicKey = $signingSecretKey->getPublicKey();

        return $this->writeKey($input, $output, $signingPublicKey);
    }
}
