<?php

/**
 * @copyright Visma Digital Commerce AS 2019
 * @license Proprietary
 * @author Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

$console = new \Symfony\Component\Console\Application("Sapient Console", "1.0.0");

$generateSigningSecretKey = new \Visma\SapientConsole\Command\GenerateSigningSecretKey();
$generateSealingSecretKey = new \Visma\SapientConsole\Command\GenerateSealingSecretKey();
$getSigningPublicKey = new \Visma\SapientConsole\Command\GetSigningPublicKey();
$getSealingPublicKey = new \Visma\SapientConsole\Command\GetSealingPublicKey();
$generateSharedEncryptionKey = new \Visma\SapientConsole\Command\GenerateSharedEncryptionKey();
$generateSharedAuthenticationKey = new \Visma\SapientConsole\Command\GenerateSharedAuthenticationKey();

$console->add($generateSigningSecretKey);
$console->add($generateSealingSecretKey);
$console->add($generateSharedEncryptionKey);
$console->add($generateSharedAuthenticationKey);
$console->add($getSigningPublicKey);
$console->add($getSealingPublicKey);

$console->run();
